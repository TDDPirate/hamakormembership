# Hamakor Membership Management Application

## This application was developed for Heroku deployment, but it should work also elsewhere.

## How to run the tests

There are two families of tests:
- test*.py: use `./manage.py test` to run them
- browser_test*.py: Start the application and a Selenium server (see below) and then use `./manage.py test --pattern 'browser_test*.py'`

### Downloading the Selenium executables
- selenium-server-standalone-*.jar: download from http://selenium-release.storage.googleapis.com/index.html?path=3.13/ - actual link is http://selenium-release.storage.googleapis.com/3.13/selenium-server-standalone-3.13.0.jar
- chromedriver: download from https://sites.google.com/a/chromium.org/chromedriver/downloads, actual link is https://chromedriver.storage.googleapis.com/2.40/chromedriver_linux64.zip

### Starting the servers
- `./chromedriver &`
- `java -jar selenium-server-standalone-*.jar &`