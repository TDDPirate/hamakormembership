from django.test import TestCase

from django.conf import settings
from django.urls.resolvers import get_resolver

class ApplicationAuditTests(TestCase):

    KNOWN_REVERSE_NAMES = ['db_csv_export',
                               'db_csv_import',
                               'login',
                               'logout',
                               'main',
                               'member_details',
                               'members_list',
                               'password_change',
                               'password_change_done',
                               'password_reset',
                               'password_reset_complete',
                               'password_reset_confirm',
                               'password_reset_done',
                               'report_auditlog',
                               'report_pay_reminders',
                               'report_voters',
                               ]

    def test_reverse_paths(self):
        """Verify that there are no surprises in the list of the reverse paths"""
        resolver = get_resolver()
        resolver._populate()  # Works for Django 2.0
        namespaces = sorted([key for key in resolver._reverse_dict.keys()])
        self.assertEqual(namespaces, ['en-us'], 'Extra namespaces were added') # NOTE: When working interactively, using './manage.py shell', the namespace is None rather than 'en-us'
        names = sorted([key for key in resolver._reverse_dict['en-us'].keys() if isinstance(key,str)])
        self.assertEqual(names, self.KNOWN_REVERSE_NAMES, 'Extra names were added')
        
