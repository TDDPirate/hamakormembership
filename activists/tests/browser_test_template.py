# Template for building browser tests using Selenium

from django.test import LiveServerTestCase
from django.contrib.auth.models import User

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys

class LoginLiveServerTestCase(LiveServerTestCase):
    SUPERUSER = 'bdfl'
    SUPER_EMAIL = 'bdfl@bdfl.org'
    SUPER_PASSWORD = 'This is the BDFL'

    def setUp(self):
        superuser = User.objects.create_superuser(username=self.SUPERUSER, email=self.SUPER_EMAIL, password=self.SUPER_PASSWORD)
        #superuser.save()  # create_superuser already saves the superuser.

        REMOTE_DRIVER = False
        if (REMOTE_DRIVER):
            # TODO: bring in arguments properly
            hostip = 'localhost'
            browser = 'CHROME'
            self.driver = webdriver.Remote(
                command_executor=('http://%(hostip)s:4444/wd/hub' % {'hostip' : hostip}),
                desired_capabilities=DesiredCapabilities.__dict__[browser])
        else:
            self.driver = webdriver.Chrome(executable_path="chromedriver")

    def tearDown(self):
        self.driver.close()

class BrowserTestsTemplate(LoginLiveServerTestCase):

    def test_selenium_standalone_is_alive(self):
        self.driver.get(self.live_server_url)
        elem = self.driver.find_element_by_id('container')
        self.assertIsNotNone(elem)

    def test_browser_login(self):
        self.driver.get(self.live_server_url)
        # Find relevant controls
        username_input_elem = self.driver.find_element_by_id('id_username')
        password_input_elem = self.driver.find_element_by_id('id_password')
        submit_elem = self.driver.find_element_by_xpath('//form/input[@type="submit"]')
        # Verify that they were found
        self.assertIsNotNone(username_input_elem)
        self.assertIsNotNone(password_input_elem)
        self.assertIsNotNone(submit_elem)
        # Manipulate
        username_input_elem.send_keys(self.SUPERUSER)
        #password_input_elem.clear()
        password_input_elem.send_keys(self.SUPER_PASSWORD)
        submit_elem.click()
        self.assertEquals(self.driver.current_url, '%s/' % self.live_server_url)
