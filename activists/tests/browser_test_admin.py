# Run browser tests on the Admin dialogs.

from .browser_test_template import LoginLiveServerTestCase
# Enhances LiveServerTestCase as follows:
# - Creates the 'bdfl' superuser.

from selenium.webdriver.common.keys import Keys

class AdminTests(LoginLiveServerTestCase):

    def setUp(self):
        super().setUp()
        
        self.driver.get(self.live_server_url)
        # Find relevant controls
        username_input_elem = self.driver.find_element_by_id('id_username')
        password_input_elem = self.driver.find_element_by_id('id_password')
        submit_elem = self.driver.find_element_by_xpath('//form/input[@type="submit"]')
        # Verify that they were found
        self.assertIsNotNone(username_input_elem)
        self.assertIsNotNone(password_input_elem)
        self.assertIsNotNone(submit_elem)
        # Manipulate
        username_input_elem.send_keys(self.SUPERUSER)
        password_input_elem.send_keys(self.SUPER_PASSWORD)
        submit_elem.click()
        # Verify that login was successful
        self.assertEquals(self.driver.current_url, '%s/' % self.live_server_url)

    def test_links_to_admin(self):
        view_and_edit_members_link = self.driver.find_element_by_link_text("View and edit members' data")
        self.assertIsNotNone(view_and_edit_members_link)
        view_and_edit_members_link.click()
        self.assertEquals(self.driver.current_url,'%s/admin/members/person/' % self.live_server_url)
