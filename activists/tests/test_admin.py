from django.test import TestCase, Client
from django.contrib.auth.models import User

class AdminFormTests(TestCase):
    def setUp(self):
        superuser = User.objects.create_user(username='bdfl')
        superuser.is_staff = True
        superuser.is_superuser = True
        superuser.save()
        #self.client = Client()  # already taken care of by TestCase
        self.client.force_login(user=superuser)

    def tearDown(self):
        self.client.logout()

    def test_members_person_add(self):
        response = self.client.get(path="/admin/members/person/add/")
        self.assertEqual(response.status_code, 200)
        decoded_response = response.content.decode()
        self.assertEqual(decoded_response[:15], '<!DOCTYPE html>')
        self.assertEqual(decoded_response.count("""<input type="submit" value="""), 3)
        self.assertEqual(decoded_response.count("""<input type="text" name="""), 15)
        self.assertEqual(decoded_response.count("""<input type="email" name="""), 1)
        self.assertEqual(decoded_response.count("""<input type="checkbox" name="""), 6)  
