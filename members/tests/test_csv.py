from django.test import TestCase
from django.conf import settings
from members.models import Person, IdType, Status, Gender
from members.views import _import_csv_file, _export_csv_file

import io
import os

SUCCESSFUL_SIX_PERSONS_IMPORT_STATUS_MESSAGE = {
    'statusmessages': [],
    'statusmsg': 'CSV import has been completed: 6 persons, 8 contact information records, 2 comment records.',
    }

DUPLICATE_USER_ID_IMPORT_STATUS_MESSAGE = {
    'statusmessages': ['Validation errors for 1:Member   בנימין-זאב הרצל (Benyamin-Zeev Herzl):\n',
                           "    {'id_member': ['Person with this Member id already exists.']}\n"
                           ],
    'statusmsg': 'CSV import has been completed: 0 persons, 0 contact information records, 0 comment records.',
    }

class CsvTests(TestCase):
    def test_import(self):
        test_csv_filename = os.path.join(settings.BASE_DIR, 'testfiles', 'six_persons.csv')
        with open(test_csv_filename) as inpcsv:
            csv_text = inpcsv.read()
        statusdict = _import_csv_file(io.StringIO(csv_text))
        self.assertEqual(len(Person.objects.all()), 6)
        person = Person.objects.get(id_member=5)
        self.assertEqual(statusdict, SUCCESSFUL_SIX_PERSONS_IMPORT_STATUS_MESSAGE)
        self.assertEqual(person.name_surname_eng, "Herzl")
        self.assertEqual(person.gender, Gender.MALE.value)
        
        # Now test what happens if we try to import duplicate user IDs
        test_csv2_filename = os.path.join(settings.BASE_DIR, 'testfiles', 'duplicate_person.csv')
        with open(test_csv2_filename) as inpcsv2:
            csv2_text = inpcsv2.read()
        statusdict2 = _import_csv_file(io.StringIO(csv2_text))
        self.maxDiff = None
        self.assertEqual(statusdict2, DUPLICATE_USER_ID_IMPORT_STATUS_MESSAGE)

    def test_export(self):
        test_csv_filename = os.path.join(settings.BASE_DIR, 'testfiles', 'six_persons.csv')
        with open(test_csv_filename) as inpcsv:
            csv_text = inpcsv.read()
        statusdict = _import_csv_file(io.StringIO(csv_text))
        self.assertEqual(statusdict, SUCCESSFUL_SIX_PERSONS_IMPORT_STATUS_MESSAGE)

        # Finished preparing the database.
        csvfile = io.StringIO()
        _export_csv_file(csvfile)
        csv_out_text = csvfile.getvalue()
        csvfile.close()
        self.assertEqual(csv_text, csv_out_text)

BAD_CSV_IMPORT_STATUS_MESSAGE = {
    'statusmessages': ['Empty E-mail address for 4:Member   דנלח לכל-החיים (BDFL For-Life)\n',
                           'An empty row was encountered and skipped\n',
                           'Validation errors for 7:Friend     ( ):\n',
                           "    {'name_surname_heb': ['Empty Hebrew surname'], 'name_firstname_heb': ['Empty Hebrew first name'], 'name_surname_eng': ['Empty English surname'], 'name_firstname_eng': ['Empty English first name']}\n",
                           'Validation errors for 8:Friend     (mName ):\n',
                           "    {'name_surname_heb': ['Empty Hebrew surname'], 'name_firstname_heb': ['Empty Hebrew first name'], 'name_surname_eng': ['Empty English surname']}\n",
                           'Validation errors for 9:Friend     ( surName):\n',
                           "    {'name_surname_heb': ['Empty Hebrew surname'], 'name_firstname_heb': ['Empty Hebrew first name'], 'name_firstname_eng': ['Empty English first name']}\n",
                           'Validation errors for 10:Friend   hName  ( ):\n',
                           "    {'name_surname_heb': ['Empty Hebrew surname'], 'name_surname_eng': ['Empty English surname'], 'name_firstname_eng': ['Empty English first name']}\n",
                           'Validation errors for 11:Friend    hSurName ( ):\n',
                           "    {'name_firstname_heb': ['Empty Hebrew first name'], 'name_surname_eng': ['Empty English surname'], 'name_firstname_eng': ['Empty English first name']}\n"
                           ],
    'statusmsg': 'CSV import has been completed: 1 persons, 1 contact information records, 1 comment records.',
    }

class CsvExtraTests(TestCase):
    """More csv tests to get better code coverage"""
    def test_import_emptyline(self):
        """Trigger CSV import errors"""
        test_csv_filename = os.path.join(settings.BASE_DIR, 'testfiles', 'bad_csv_import.csv')
        with open(test_csv_filename) as inpcsv:
            csv_text = inpcsv.read()
        statusdict = _import_csv_file(io.StringIO(csv_text))
        self.assertEqual(len(Person.objects.all()), 1)
        self.assertEqual(statusdict, BAD_CSV_IMPORT_STATUS_MESSAGE)
# End of test_csv.py
