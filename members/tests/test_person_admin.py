# Test the Person editing from in Admin.
# See: https://github.com/django/django/blob/master/tests/modeladmin/tests.py

from django.test import TestCase
from django.contrib import admin
from django.contrib.admin.sites import AdminSite

from members.models import Person, IdType, Status, Gender
from members.admin import PersonAdmin



class MockRequest:
    pass

class MockSuperUser:
    def has_perm(self, perm):
        return True

request = MockRequest()
request.user = MockSuperUser()



class PersonAdminTests(TestCase):

    def setUp(self):
        self.site = AdminSite()

    def test_modeladmin_str(self):
        self.assertEqual(str(admin.ModelAdmin(Person, self.site)), 'members.ModelAdmin')
