from django.test import TestCase
from members.models import Person, ContactType, Contact, Comment

# Test refactorings

# Model field choices - verify unchanged functionality after refactoring.

class EnumRefactoringTests(TestCase):

    def test_person_id_type_choices(self):
        self.assertEqual(Person._meta._forward_fields_map['id_type'].flatchoices,
            [
                ('T', 'Teudat zehut'),
                ('P', 'Passport'),
            ])

    def test_person_status_choices(self):
        self.assertEqual(Person._meta._forward_fields_map['status'].flatchoices,
            [
                (1, 'Member'),
                (2, 'Friend/former member'),
                (3, 'Former member'),
                (4, 'Friend'),
            ])

    def test_person_gender_choices(self):
        self.assertEqual(Person._meta._forward_fields_map['gender'].flatchoices,
            [
                ('M', 'Male'),
                ('F', 'Female'),
                ('O', 'Other'),
            ])

# End of test_refactorings.py
