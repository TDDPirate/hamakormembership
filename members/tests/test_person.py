from django.test import TestCase
from members.models import Person, IdType, Status, Gender

# More detailed tests of the Person model

class PersonTests(TestCase):

    def test_member(self):
        person = Person(status=Status.ACTIVEMEMBER)
        self.assertTrue(person.voting())
        self.assertTrue(person.retain_data())

    def test_formermember_friend(self):
        person = Person(status=Status.FORMERMEMBER_FRIEND)
        self.assertFalse(person.voting())
        self.assertTrue(person.retain_data())

    def test_formermember_nonfriend(self):
        person = Person(status=Status.FORMERMEMBER_NONFRIEND)
        self.assertFalse(person.voting())
        self.assertTrue(person.retain_data())

    def test_friend(self):
        person = Person(status=Status.FRIEND)
        self.assertFalse(person.voting())
        self.assertFalse(person.retain_data())

class PersonFieldTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        Person.objects.create(
            id_official="98765",
            id_type=IdType.TEUDATZEHUT,
            email="1st@person.self",
            name_surname_heb="ראשון",
            name_firstname_heb="אדם",
            name_surname_eng="First",
            name_firstname_eng="Adam",
            status=Status.ACTIVEMEMBER,
            # membership_fee_end
            # membership_start
            address_street_number="Allenby 33",
            address_city="Altnoycity",
            address_zipcode="777",
            address_country="Neverland",
            gender=Gender.OTHER,
            # birthday
            )

    def test_id_member_field(self):
        person=Person.objects.get(id_member=1)
        field = person._meta.get_field('id_member')
        self.assertEquals(field.verbose_name, 'member id')

    def test_id_official_field(self):
        person=Person.objects.get(id_member=1)
        field = person._meta.get_field('id_official')
        self.assertEquals(field.verbose_name, 'government id')
        self.assertEquals(field.max_length, 12)

    def test_id_type_field(self):
        person=Person.objects.get(id_member=1)
        field = person._meta.get_field('id_type')
        self.assertEquals(field.verbose_name, 'id type')
        self.assertEquals(field.max_length, 2)

    def test_email_field(self):
        person=Person.objects.get(id_member=1)
        field = person._meta.get_field('email')
        self.assertEquals(field.verbose_name, 'official email')

    # TODO: test more fields

    # Person.get_absolute_url() is not defined, yet.
    #def test_get_absolute_url(self):
    #    person=Person.objects.get(id_member=1)
    #    self.assertEquals(person.get_absolute_url(), '/admin/members/person/1')

# End of test_person.py
