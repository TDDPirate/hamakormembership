from django.test import TestCase
from members.models import Person, ContactType, Contact, Comment

# Basic object creation and saving

class BasicTests(TestCase):
    def test_person(self):
        person = Person(id_official="1234", email="1234@5678.com", name_surname_eng="Palmoni", name_firstname_eng="Ploni")
        person.save()
        self.assertEqual(len(Person.objects.all()), 1)
        self.assertEqual(str(person), 'Status.FRIEND. 1:   (Ploni Palmoni) 1234@5678.com')

    def test_contact(self):
        person = Person(id_official="1234", email="1234@5678.com", name_surname_eng="Palmoni", name_firstname_eng="Ploni")
        person.save()
        contact = Contact.create_if_nonempty(contact_type_name=ContactType.CT_FAX, person=person, contact_data="+972-3-0000000")
        self.assertEqual(len(Contact.objects.all()), 1)
        self.assertEqual(len(ContactType.objects.all()), 1)
        self.assertEqual(str(contact), 'Ploni Palmoni: FAX = +972-3-0000000')

    def test_comment(self):
        person = Person(id_official="1234", email="1234@5678.com", name_surname_eng="Palmoni", name_firstname_eng="Ploni")
        person.save()
        comment = Comment.create_if_nonempty(person=person,cmt_text="Comment 1 on Ploni Palmoni")
        self.assertEqual(len(Comment.objects.all()), 1)
        self.assertRegex(str(comment), r'^Ploni Palmoni: \d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d\.\d\d\d\d\d\d\+00:00: Comment 1 on Ploni Palmoni$')
